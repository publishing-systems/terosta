/* Copyright (C) 2017-2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroEvent.h
 * @author Stephan Kreutzer
 * @since 2021-10-27
 */

#ifndef _TEROSTA_TEROEVENT_H
#define _TEROSTA_TEROEVENT_H

#include <string>

namespace terosta
{

class TeroEvent
{
public:
    TeroEvent(const std::string& strFunctionNameCurrent,
              const std::string& strData,
              const std::string& strFunctionNameNext,
              const std::string& strPatternNameMatch);

public:
    const std::string& getFunctionNameCurrent();
    const std::string& getData();

public:
    const std::string& getFunctionNameNext();
    const std::string& getPatternNameMatch();

protected:
    std::string m_strFunctionNameCurrent;
    std::string m_strData;

    std::string m_strFunctionNameNext;
    std::string m_strPatternNameMatch;

};

}

#endif
