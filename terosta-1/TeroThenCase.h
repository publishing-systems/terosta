/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroThenCase.h
 * @author Stephan Kreutzer
 * @since 2021-08-15
 */

#ifndef _TEROSTA_TEROTHENCASE_H
#define _TEROSTA_TEROTHENCASE_H


#include <string>
#include <stdexcept>
#include <memory>
#include <list>
#include "TeroPattern.h"


namespace terosta
{

class TeroThenCase
{
public:
    TeroThenCase(const std::list<std::shared_ptr<TeroPattern>>& pPatterns,
                 const std::string& strThenCaseCode,
                 const std::string& strThenCaseFunction);

public:
    const std::list<std::shared_ptr<TeroPattern>>& GetPatterns();
    const std::string& GetThenCaseCode();
    const std::string& GetThenCaseFunction();

protected:
    std::list<std::shared_ptr<TeroPattern>> m_pPatterns;
    std::string m_strThenCaseCode;
    std::string m_strThenCaseFunction;

};

}

#endif
