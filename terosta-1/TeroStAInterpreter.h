/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroStAInterpreter.h
 * @author Stephan Kreutzer
 * @since 2021-06-23
 */

#ifndef _TEROSTA_TEROSTAINTERPRETER_H
#define _TEROSTA_TEROSTAINTERPRETER_H

#include <istream>
#include <string>
#include <locale>
#include <memory>
#include <map>
#include <stdexcept>
#include <queue>
#include <sstream>
#include "TeroFunction.h"
#include "TeroPattern.h"
#include "TeroInputStreamInterface.h"
#include "TeroEvent.h"

namespace terosta
{

class TeroStAInterpreter
{
public:
    TeroStAInterpreter(const std::string& strCodeDirectoryPath,
                       const std::string& strMainFunction,
                       TeroInputStreamInterface& aStream,
                       bool bAsync = false);
    ~TeroStAInterpreter();

public:
    bool hasNext();
    std::unique_ptr<TeroEvent> nextEvent();

protected:
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator loadFlow();
    std::map<std::string, std::shared_ptr<TeroPattern>>::iterator loadPattern(const std::string& strPatternName);

protected:
    int handleSection(std::istream& aStream,
                      std::unique_ptr<std::string>& pResult,
                      char cByteEnd);

protected:
    int parse();

protected:
    bool m_bHasNextCalled;
    std::queue<std::unique_ptr<TeroEvent>> m_aEvents;
    TeroInputStreamInterface& m_aStream;

protected:
    std::map<std::string, std::unique_ptr<TeroFunction>> m_aFunctions;
    std::map<std::string, std::shared_ptr<TeroPattern>> m_aPatterns;

protected:
    std::string m_strCodeDirectoryPath;
    std::locale m_aLocale;
    bool m_bAsync = false;

protected:
    std::string m_strFunctionCurrent;
    std::string m_strFunctionPrevious;
    std::stringstream m_strCollector;
};

}

#endif
