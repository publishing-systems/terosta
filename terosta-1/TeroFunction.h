/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroFunction.h
 * @author Stephan Kreutzer
 * @since 2021-07-18
 */

#ifndef _TEROSTA_TEROFUNCTION_H
#define _TEROSTA_TEROFUNCTION_H


#include "TeroThenCase.h"
#include <string>
#include <list>
#include <memory>
#include <stdexcept>


namespace terosta
{

class TeroFunction
{
public:
    TeroFunction(const std::string& strName,
                 std::unique_ptr<std::list<std::unique_ptr<TeroThenCase>>> pThenCases,
                 const std::string& strElseCaseCode,
                 const std::string& strElseCaseFunction,
                 bool bHasRetain);

public:
    const std::list<std::unique_ptr<TeroThenCase>>& GetThenCases();
    const std::string& GetElseCaseCode();
    const std::string& GetElseCaseFunction();
    bool GetHasRetain();

protected:
    std::string m_strName;
    std::unique_ptr<std::list<std::unique_ptr<TeroThenCase>>> m_pThenCases;
    std::string m_strElseCaseCode;
    std::string m_strElseCaseFunction;
    bool m_bHasRetain = false;

};

}

#endif
