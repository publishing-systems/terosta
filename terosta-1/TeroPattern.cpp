/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroPattern.cpp
 * @author Stephan Kreutzer
 * @since 2021-09-08
 */


#include <stdexcept>
#include "TeroPattern.h"


namespace terosta
{

TeroPattern::TeroPattern(const std::string& strPatternName,
                         std::unique_ptr<std::list<std::unique_ptr<TeroRule>>>& pRules):
  m_strPatternName(strPatternName),
  m_pRules(std::move(pRules))
{
    if (m_strPatternName.empty() == true)
    {
        throw new std::invalid_argument("TeroPattern::TeroPattern(): Empty pattern name string passed.");
    }

    if (m_pRules == nullptr)
    {
        throw new std::invalid_argument("TeroPattern::TeroPattern() with pRules == nullptr.");
    }
}

const std::string& TeroPattern::GetPatternName()
{
    return m_strPatternName;
}

const std::list<std::unique_ptr<TeroRule>>& TeroPattern::GetRules()
{
    return *m_pRules;
}

}
