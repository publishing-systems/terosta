/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/terosta.cpp
 * @author Stephan Kreutzer
 * @since 2021-10-27
 */

#include "TeroStAInterpreter.h"
#include "TeroInputStreamStd.h"
#include "TeroEvent.h"
#include <memory>
#include <iostream>
#include <fstream>
#include <stdexcept>



int main(int argc, char* argv[])
{
    std::cout << "terosta Copyright (C) 2021-2023 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/publishing-systems/TeroStA/ and the project\n"
              << "website https://hypertext-systems.org.\n"
              << std::endl;

    if (argc < 4)
    {
        std::cout << "Usage:\n\n\tterosta <code-directory-path> <main-function> <input-file>\n" << std::endl;
        return 1;
    }

    std::unique_ptr<std::ifstream> pInputStream = nullptr;

    try
    {
        pInputStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pInputStream->open(argv[3], std::ios::in | std::ios::binary);

        if (pInputStream->is_open() != true)
        {
            std::cerr << "Couldn't open input file \"" << argv[3] << "\"." << std::endl;
            return 1;
        }

        terosta::TeroInputStreamStd aInputStream(*pInputStream);

        terosta::TeroStAInterpreter aInterpreter(argv[1], argv[2], aInputStream, false);

        // Instead of looking at Events sequentially, one could
        // also implement a "parse tree" to react to TeroEvents,
        // so writing state machines can be avoided because of
        // the implicit call tree context.
        while (aInterpreter.hasNext() == true)
        {
            std::unique_ptr<terosta::TeroEvent> pEvent = aInterpreter.nextEvent();

            if (pEvent->getFunctionNameCurrent() == "InClassName")
            {
                std::cout << pEvent->getData();

                do
                {
                    if (pEvent->getPatternNameMatch() == "CppScopeStart" ||
                        pEvent->getPatternNameMatch() == "CppInheritanceColon")
                    {
                        break;
                    }
                    else if (pEvent->getPatternNameMatch() != "Whitespace")
                    {
                        std::cout << " |X";
                        break;
                    }

                    if (aInterpreter.hasNext() != true)
                    {
                        break;
                    }

                    pEvent = aInterpreter.nextEvent();

                    if (pEvent->getFunctionNameCurrent() != "InClassNamePost")
                    {
                        std::cout << " |X";
                        break;
                    }

                } while (true);

                std::cout << std::endl;
            }
        }

        pInputStream->close();
        pInputStream.reset(nullptr);
    }
    catch (std::exception* pException)
    {
        std::cerr << "Exception: " << pException->what() << std::endl;

        if (pInputStream != nullptr)
        {
            if (pInputStream->is_open() == true)
            {
                pInputStream->close();
            }
        }

        return 1;
    }

    return 0;
}
