/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroCharacter.cpp
 * @author Stephan Kreutzer
 * @since 2021-09-16
 */


#include "TeroCharacter.h"


namespace terosta
{

TeroCharacter::TeroCharacter(const char& cCharacter):
  m_cCharacter(cCharacter)
{

}

int TeroCharacter::Compare(const char& cByte)
{
    if (m_cCharacter == cByte)
    {
        return TeroRule::RETURNVALUE_COMPARE_MATCH;
    }
    else
    {
        return TeroRule::RETURNVALUE_COMPARE_MISMATCH;
    }
}

int TeroCharacter::Reset()
{
    return 0;
}

}
