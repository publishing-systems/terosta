/* Copyright (C) 2017-2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroEvent.cpp
 * @author Stephan Kreutzer
 * @since 2021-10-27
 */

#include "TeroEvent.h"

namespace terosta
{

TeroEvent::TeroEvent(const std::string& strFunctionNameCurrent,
                     const std::string& strData,
                     const std::string& strFunctionNameNext,
                     const std::string& strPatternNameMatch):
  m_strFunctionNameCurrent(strFunctionNameCurrent),
  m_strData(strData),
  m_strFunctionNameNext(strFunctionNameNext),
  m_strPatternNameMatch(strPatternNameMatch)
{

}

const std::string& TeroEvent::getFunctionNameCurrent()
{
    return m_strFunctionNameCurrent;
}

const std::string& TeroEvent::getData()
{
    return m_strData;
}

const std::string& TeroEvent::getFunctionNameNext()
{
    return m_strFunctionNameNext;
}

const std::string& TeroEvent::getPatternNameMatch()
{
    return m_strPatternNameMatch;
}

}
