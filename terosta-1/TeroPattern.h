/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroPattern.h
 * @author Stephan Kreutzer
 * @since 2021-09-08
 */

#ifndef _TEROSTA_TEROPATTERN_H
#define _TEROSTA_TEROPATTERN_H


#include <string>
#include <stdexcept>
#include <memory>
#include <list>
#include "TeroRule.h"


namespace terosta
{

class TeroPattern
{
public:
    TeroPattern(const std::string& strPatternName,
                std::unique_ptr<std::list<std::unique_ptr<TeroRule>>>& pRules);

public:
    const std::string& GetPatternName();
    const std::list<std::unique_ptr<TeroRule>>& GetRules();

protected:
    std::string m_strPatternName;
    std::unique_ptr<std::list<std::unique_ptr<TeroRule>>> m_pRules;

};

}

#endif
