/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroRule.h
 * @author Stephan Kreutzer
 * @since 2021-09-16
 */

#ifndef _TEROSTA_TERORULE_H
#define _TEROSTA_TERORULE_H



namespace terosta
{

class TeroRule
{
public:
    virtual int Compare(const char& cByte) = 0;

    static const int RETURNVALUE_COMPARE_MATCHING = 1;
    static const int RETURNVALUE_COMPARE_MATCH = 2;
    static const int RETURNVALUE_COMPARE_MISMATCH = 3;

public:
    virtual int Reset() = 0;

};

}

#endif
