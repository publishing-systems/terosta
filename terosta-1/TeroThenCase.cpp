/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroThenCase.cpp
 * @author Stephan Kreutzer
 * @since 2021-08-15
 */


#include <stdexcept>
#include "TeroThenCase.h"


namespace terosta
{

TeroThenCase::TeroThenCase(const std::list<std::shared_ptr<TeroPattern>>& pPatterns,
                           const std::string& strThenCaseCode,
                           const std::string& strThenCaseFunction):
  m_pPatterns(pPatterns),
  m_strThenCaseCode(strThenCaseCode),
  m_strThenCaseFunction(strThenCaseFunction)
{
    if (m_pPatterns.empty() == true)
    {
        throw new std::invalid_argument("TeroThenCase::TeroThenCase(): Empty pattern list passed.");
    }

    if (m_strThenCaseFunction.empty() == true)
    {
        throw new std::invalid_argument("TeroThenCase::TeroThenCase(): Empty then-case function string passed.");
    }
}

const std::list<std::shared_ptr<TeroPattern>>& TeroThenCase::GetPatterns()
{
    return m_pPatterns;
}

const std::string& TeroThenCase::GetThenCaseCode()
{
    return m_strThenCaseCode;
}

const std::string& TeroThenCase::GetThenCaseFunction()
{
    return m_strThenCaseFunction;
}

}
