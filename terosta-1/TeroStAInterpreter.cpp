/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroStAInterpreter.cpp
 * @todo There's no reason for the code to deal with function and pattern names
 *     directly other than reporting, so could represent these as mere integer handles
 *     (to speed up comparison etc.).
 * @author Stephan Kreutzer
 * @since 2021-06-23
 */

#include "TeroStAInterpreter.h"
#include "TeroThenCase.h"
#include "TeroRule.h"
#include "TeroSequence.h"
#include "TeroCharacter.h"
#include "TeroEvent.h"
#include <iomanip>
#include <fstream>
#include <vector>
#include <unordered_set>

namespace terosta
{

/**
 * @param[in] bAsync If set to true, individual match attempt events will be reported.
 */
TeroStAInterpreter::TeroStAInterpreter(const std::string& strCodeDirectoryPath,
                                       const std::string& strMainFunction,
                                       TeroInputStreamInterface& aStream,
                                       bool bAsync):
  m_bHasNextCalled(false),
  m_aStream(aStream),
  m_strCodeDirectoryPath(strCodeDirectoryPath),
  m_bAsync(bAsync),
  m_strFunctionCurrent(strMainFunction)
{
    if (m_strFunctionCurrent.empty() == true)
    {
        throw new std::runtime_error("Empty main function name.");
    }
}

TeroStAInterpreter::~TeroStAInterpreter()
{

}

bool TeroStAInterpreter::hasNext()
{
    if (m_aEvents.size() > 0)
    {
        return true;
    }

    if (m_bHasNextCalled == true)
    {
        return false;
    }
    else
    {
        m_bHasNextCalled = true;
    }

    if (m_aStream.eof() == true)
    {
        return false;
    }


    int nResult = parse();

    do
    {
        if (nResult == 2)
        {
            nResult = parse();
            continue;
        }
        else if (nResult == 1)
        {
            break;
        }
        else if (nResult == 0)
        {
            break;
        }
        else if (nResult < 0)
        {
            return false;
        }
        else
        {
            
        }

    } while (true);

    return m_aEvents.size() > 0;
}

std::unique_ptr<TeroEvent> TeroStAInterpreter::nextEvent()
{
    if (m_aEvents.size() <= 0 &&
        m_bHasNextCalled == false)
    {
        if (hasNext() != true)
        {
            throw new std::logic_error("Attempted TeroStAInterpreter::nextEvent() while there isn't one instead of checking TeroStAInterpreter::hasNext() first.");
        }
    }

    m_bHasNextCalled = false;

    if (m_aEvents.size() <= 0)
    {
        throw new std::logic_error("TeroStAInterpreter::nextEvent() while there isn't one, ignoring TeroStAInterpreter::hasNext() == false.");
    }

    std::unique_ptr<TeroEvent> pEvent = std::move(m_aEvents.front());
    m_aEvents.pop();

    return pEvent;
}

std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator TeroStAInterpreter::loadFlow()
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = m_aFunctions.find(m_strFunctionCurrent);

    if (iterFunction != m_aFunctions.end())
    {
        return iterFunction;
    }

    std::string strCodeFilePath;
    strCodeFilePath += m_strCodeDirectoryPath;
    strCodeFilePath += "flow/";
    strCodeFilePath += m_strFunctionCurrent;
    strCodeFilePath += ".teroflow";

    std::vector<std::unique_ptr<std::string>> aSections;
    bool bHasRetain = false;

    std::unique_ptr<std::ifstream> pCodeStream = nullptr;

    try
    {
        pCodeStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pCodeStream->open(strCodeFilePath, std::ios::in | std::ios::binary);

        if (pCodeStream->is_open() != true)
        {
            std::stringstream aMessage;
            aMessage << "Couldn't open flow/function/code file \"" << strCodeFilePath << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        char cByte = '\0';

        do
        {
            pCodeStream->get(cByte);

            if (pCodeStream->eof() == true)
            {
                break;
            }

            if (pCodeStream->bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == '(' ||
                cByte == '[')
            {
                char cByteEnd = ')';

                if (cByte == '[')
                {
                    cByteEnd = ']';
                    bHasRetain = true;
                }

                std::unique_ptr<std::string> pSection = nullptr;

                handleSection(*pCodeStream, pSection, cByteEnd);

                aSections.push_back(std::move(pSection));
            }
            else
            {
                /*
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                        << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                        << ").";
                throw new std::runtime_error(aMessage.str());
                */
            }

        } while (true);

        pCodeStream->close();
        pCodeStream.reset(nullptr);
    }
    catch (std::exception* pException)
    {
        if (pCodeStream != nullptr)
        {
            if (pCodeStream->is_open() == true)
            {
                pCodeStream->close();
            }
        }

        throw pException;
    }

    std::size_t nSectionCount = aSections.size();

    if (nSectionCount < 6U)
    {
        throw new std::runtime_error("Less than the minimum total of at least 6 sections.");
    }

    {
        int nSectionCountThenCases = (nSectionCount - 3U);

        if (nSectionCountThenCases % 3U != 0U)
        {
            throw new std::runtime_error("Then-case incomplete, section(s) missing.");
        }
    }

    if (aSections.at(0)->empty() == true)
    {
        throw new std::runtime_error("Section 1 is empty.");
    }

    if (*(aSections.at(0)) != m_strFunctionCurrent)
    {
        std::stringstream aMessage;
        aMessage << "Function name \""
                 << *(aSections.at(0))
                 << "\" does mismatch expected code file name \""
                 << m_strFunctionCurrent
                 << "\" of path \""
                 << strCodeFilePath
                 << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    std::unique_ptr<std::list<std::unique_ptr<TeroThenCase>>> pThenCases(new std::list<std::unique_ptr<TeroThenCase>>);
    /** @todo This is just to check for duplicates. Better replace by efficient pThenCases list lookup? */
    std::unordered_set<std::string> aThenCaseList;

    for (std::size_t i = 1U, max = (nSectionCount - 2U); i < max; i += 3U)
    {
        if (aSections.at(i)->empty() == true)
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << i << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }

        if (aThenCaseList.find(*(aSections.at(i))) == aThenCaseList.end())
        {
            aThenCaseList.insert(*(aSections.at(i)));
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", pattern \"" << *(aSections.at(i)) << "\" is defined more than once.";
            throw new std::runtime_error(aMessage.str());
        }

        if (aSections.at(i + 2U)->empty() == true)
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << (i + 2U) << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }

        /** @todo This should use the very same reader/loader, but taking a different
          * section definition as the parameter, and filling a generic representation,
          * be it a TeroFunction or TeroPattern! */
        std::map<std::string, std::shared_ptr<TeroPattern>>::iterator iterPattern = loadPattern(*(aSections.at(i)));

        if (iterPattern == m_aPatterns.end())
        {
            std::stringstream aMessage;
            aMessage << "Use of unknown pattern \"" << *(aSections.at(i)) << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        // In theory, would support multiple patterns for a then-case, but notation
        // currently supports only one, probably for the better (is cleaner?).
        std::list<std::shared_ptr<TeroPattern>> aPatterns;
        aPatterns.push_back(iterPattern->second);

        /** @todo Pass/move std::unique_ptr<std::string> into TeroThenCase instead of copying? */
        std::unique_ptr<TeroThenCase> pThenCase(new TeroThenCase(aPatterns,
                                                                 *(aSections.at(i + 1U)),
                                                                 *(aSections.at(i + 2U))));

        pThenCases->push_back(std::move(pThenCase));
    }

    if (aSections.at(nSectionCount - 1U)->empty() == true)
    {
        std::stringstream aMessage;
        aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << (nSectionCount - 1U) << " is empty.";
        throw new std::runtime_error(aMessage.str());
    }

    std::unique_ptr<TeroFunction> pFunction(new TeroFunction(*(aSections.at(0)),
                                                             std::move(pThenCases),
                                                             *(aSections.at(nSectionCount - 2U)),
                                                             *(aSections.at(nSectionCount - 1U)),
                                                             bHasRetain));

    return m_aFunctions.insert(std::pair<std::string, std::unique_ptr<TeroFunction>>(*(aSections.at(0)), std::move(pFunction))).first;
}

std::map<std::string, std::shared_ptr<TeroPattern>>::iterator TeroStAInterpreter::loadPattern(const std::string& strPatternName)
{
    std::map<std::string, std::shared_ptr<TeroPattern>>::iterator iterPattern = m_aPatterns.find(strPatternName);

    if (iterPattern != m_aPatterns.end())
    {
        return iterPattern;
    }

    if (strPatternName.empty() == true)
    {
        throw new std::runtime_error("Empty pattern name.");
    }

    std::string strPatternFilePath;
    strPatternFilePath += m_strCodeDirectoryPath;
    strPatternFilePath += "patterns/";
    strPatternFilePath += strPatternName;
    strPatternFilePath += ".teroptrn";

    std::vector<std::unique_ptr<std::string>> aSections;

    std::unique_ptr<std::ifstream> pPatternStream = nullptr;

    try
    {
        pPatternStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pPatternStream->open(strPatternFilePath, std::ios::in | std::ios::binary);

        if (pPatternStream->is_open() != true)
        {
            std::stringstream aMessage;
            aMessage << "Couldn't open pattern file \"" << strPatternFilePath << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        char cByte = '\0';

        do
        {
            pPatternStream->get(cByte);

            if (pPatternStream->eof() == true)
            {
                break;
            }

            if (pPatternStream->bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == '(')
            {
                std::unique_ptr<std::string> pSection = nullptr;

                handleSection(*pPatternStream, pSection, ')');

                aSections.push_back(std::move(pSection));
            }
            else
            {
                /*
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                        << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                        << ").";
                throw new std::runtime_error(aMessage.str());
                */
            }

        } while (true);

        pPatternStream->close();
        pPatternStream.reset(nullptr);
    }
    catch (std::exception* pException)
    {
        if (pPatternStream != nullptr)
        {
            if (pPatternStream->is_open() == true)
            {
                pPatternStream->close();
            }
        }

        throw pException;
    }

    std::size_t nSectionCount = aSections.size();

    if (nSectionCount < 2U)
    {
        throw new std::runtime_error("Less than the minimum total of at least 2 sections.");
    }

    if (aSections.at(0)->empty() == true)
    {
        throw new std::runtime_error("Section 1 is empty.");
    }

    if (*(aSections.at(0)) != strPatternName)
    {
        std::stringstream aMessage;
        aMessage << "Pattern name \""
                 << *(aSections.at(0))
                 << "\" does mismatch expected pattern file name \""
                 << strPatternName
                 << "\" of path \""
                 << strPatternFilePath
                 << "\".";
        throw new std::runtime_error(aMessage.str());
    }


    std::unique_ptr<std::list<std::unique_ptr<TeroRule>>> pRules(new std::list<std::unique_ptr<TeroRule>>);
    /** @todo This is just to check for duplicates. Better replace by efficient pRules list lookup? */
    std::unordered_set<std::string> aSectionList;

    for (std::size_t i = 1U; i < nSectionCount; i++)
    {
        if (aSections.at(i)->empty() == true)
        {
            std::stringstream aMessage;
            aMessage << "In pattern \"" << *(aSections.at(0)) << "\", section/sequence " << i << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }

        if (aSectionList.find(*(aSections.at(i))) == aSectionList.end())
        {
            aSectionList.insert(*(aSections.at(i)));
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "In pattern \"" << *(aSections.at(0)) << "\", sequence \"" << *(aSections.at(i)) << "\" is defined more than once.";
            throw new std::runtime_error(aMessage.str());
        }

        std::size_t nSectionLength = aSections.at(i)->size();

        if (nSectionLength == 1U)
        {
            pRules->push_back(std::unique_ptr<TeroCharacter>(new TeroCharacter(aSections.at(i)->at(0))));
        }
        else if (nSectionLength > 1U)
        {
            pRules->push_back(std::unique_ptr<TeroSequence>(new TeroSequence(*(aSections.at(i)))));
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "In pattern \"" << *(aSections.at(0)) << "\", section/sequence " << i << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }
    }

    std::shared_ptr<TeroPattern> pPattern(new TeroPattern(*(aSections.at(0)),
                                                          pRules));

    return m_aPatterns.insert(std::pair<std::string, std::shared_ptr<TeroPattern>>(*(aSections.at(0)), std::move(pPattern))).first;
}

int TeroStAInterpreter::handleSection(std::istream& aStream,
                                      std::unique_ptr<std::string>& pResult,
                                      char cByteEnd)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("TeroStAInterpreter::handleSection() with pResult != nullptr.");
    }

    std::stringstream strSection;
    char cByte = '\0';

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream while attempting to read the section.");
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == cByteEnd)
        {
            pResult.reset(new std::string);
            *pResult = strSection.str();
            return 0;
        }
        else
        {
            strSection << cByte;
        }

    } while (true);
}

int TeroStAInterpreter::parse()
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = loadFlow();

    if (iterFunction == m_aFunctions.end())
    {
        std::stringstream aMessage;
        aMessage << "Call of unknown function \"" << m_strFunctionCurrent << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    const std::list<std::unique_ptr<TeroThenCase>>& aThenCaseSource = iterFunction->second->GetThenCases();

    /** @todo The matching of multiple then-cases, multiple patterns, multiple rules is implemented very
      * inefficiently (building/resetting aThenCases and removing candidates every time), but initially
      * left this way for clarity of and illustrating the concept. State automaton could stay at the current
      * state/function if no match/transition is reached. */
    // <then-case-position, pattern-position, rule-position, rule>
    // std::map<std::size_t, TeroRule&> just because std::list<TeroRule&> internally based on pointers, not supporting references.
    std::map<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>> aThenCases;

    {
        std::size_t i = 0U;

        for (std::list<std::unique_ptr<TeroThenCase>>::const_iterator iterThenCaseSource = aThenCaseSource.begin();
             iterThenCaseSource != aThenCaseSource.end();
             iterThenCaseSource++)
        {
            std::map<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>>::iterator iterThenCaseTarget = aThenCases.insert(std::pair<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>>(i, std::map<std::size_t, std::map<std::size_t, TeroRule&>>())).first;

            const std::list<std::shared_ptr<TeroPattern>>& aPatterns = (*iterThenCaseSource)->GetPatterns();
            std::size_t j = 0U;

            for (std::list<std::shared_ptr<TeroPattern>>::const_iterator iterPatternSource = aPatterns.begin();
                 iterPatternSource != aPatterns.end();
                 iterPatternSource++)
            {
                std::map<std::size_t, std::map<std::size_t, TeroRule&>>::iterator iterPatternTarget = iterThenCaseTarget->second.insert(std::pair<std::size_t, std::map<std::size_t, TeroRule&>>(j, std::map<std::size_t, TeroRule&>())).first;

                const std::list<std::unique_ptr<TeroRule>>& aRules = (*iterPatternSource)->GetRules();
                std::size_t k = 0U;

                for (std::list<std::unique_ptr<TeroRule>>::const_iterator iterRuleSource = aRules.begin();
                    iterRuleSource != aRules.end();
                    iterRuleSource++)
                {
                    (*iterRuleSource)->Reset();
                    iterPatternTarget->second.insert(std::pair<std::size_t, TeroRule&>(k, *(*iterRuleSource)));

                    ++k;
                }

                ++j;
            }

            ++i;
        }
    }


    std::string strBuffer;
    char cByte = '\0';
    std::map<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>>::const_iterator iterMatch = aThenCases.end();
    std::size_t nPatternMatch = -1;

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            if (m_bAsync == false &&
                m_strFunctionPrevious == m_strFunctionCurrent)
            {
                std::unique_ptr<TeroEvent> pEvent(new TeroEvent(m_strFunctionCurrent,
                                                                m_strCollector.str(),
                                                                "",
                                                                ""));
                m_aEvents.push(std::move(pEvent));

                m_strCollector.str("");
                m_strCollector.clear();
            }

            return 1;
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        strBuffer += cByte;


        std::map<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>>::const_iterator iterDeleteThenCase = aThenCases.end();

        for (std::map<std::size_t, std::map<std::size_t, std::map<std::size_t, TeroRule&>>>::iterator iterThenCase = aThenCases.begin();
             iterThenCase != aThenCases.end();
             iterThenCase++)
        {
            if (iterDeleteThenCase != aThenCases.end())
            {
                aThenCases.erase(iterDeleteThenCase);
                iterDeleteThenCase = aThenCases.end();
            }

            std::map<std::size_t, std::map<std::size_t, TeroRule&>>& aPatterns = iterThenCase->second;
            std::map<std::size_t, std::map<std::size_t, TeroRule&>>::const_iterator iterDeletePattern = aPatterns.end();

            for (std::map<std::size_t, std::map<std::size_t, TeroRule&>>::iterator iterPattern = aPatterns.begin();
                 iterPattern != aPatterns.end();
                 iterPattern++)
            {
                if (iterDeletePattern != aPatterns.end())
                {
                    aPatterns.erase(iterDeletePattern);
                    iterDeletePattern = aPatterns.end();
                }

                std::map<std::size_t, TeroRule&>& aRules = iterPattern->second;
                std::map<std::size_t, TeroRule&>::const_iterator iterDeleteRule = aRules.end();

                for (std::map<std::size_t, TeroRule&>::const_iterator iterRule = aRules.begin();
                     iterRule != aRules.end();
                     iterRule++)
                {
                    if (iterDeleteRule != aRules.end())
                    {
                        aRules.erase(iterDeleteRule);
                        iterDeleteRule = aRules.end();
                    }

                    int nComparisonResult = iterRule->second.Compare(cByte);

                    if (nComparisonResult == TeroRule::RETURNVALUE_COMPARE_MATCH)
                    {
                        iterMatch = iterThenCase;

                        nPatternMatch = iterPattern->first;

                        break;
                    }
                    else if (nComparisonResult == TeroRule::RETURNVALUE_COMPARE_MATCHING)
                    {
                        continue;
                    }
                    else if (nComparisonResult == TeroRule::RETURNVALUE_COMPARE_MISMATCH)
                    {
                        iterDeleteRule = iterRule;
                        continue;
                    }
                    else
                    {
                        
                    }
                }

                if (iterMatch != aThenCases.end())
                {
                    break;
                }

                if (iterDeleteRule != aRules.end())
                {
                    aRules.erase(iterDeleteRule);
                    iterDeleteRule = aRules.end();
                }

                if (aRules.empty() == true)
                {
                    iterDeletePattern = iterPattern;
                    continue;
                }
            }

            if (iterMatch != aThenCases.end())
            {
                break;
            }

            if (iterDeletePattern != aPatterns.end())
            {
                aPatterns.erase(iterDeletePattern);
                iterDeletePattern = aPatterns.end();
            }

            if (aPatterns.empty() == true)
            {
                iterDeleteThenCase = iterThenCase;
                continue;
            }
        }

        if (iterMatch != aThenCases.end())
        {
            break;
        }

        if (iterDeleteThenCase != aThenCases.end())
        {
            aThenCases.erase(iterDeleteThenCase);
            iterDeleteThenCase = aThenCases.end();
        }

        if (aThenCases.empty() == true)
        {
            break;
        }

    } while (true);


    m_strFunctionPrevious = m_strFunctionCurrent;
    std::string strPatternNameMatch;

    if (iterMatch != aThenCases.end())
    {
        std::list<std::unique_ptr<TeroThenCase>>::const_iterator iterThenCase = aThenCaseSource.end();

        {
            std::list<std::unique_ptr<TeroThenCase>>::const_iterator iter = aThenCaseSource.begin();

            for (std::size_t i = 0U, max = iterMatch->first; i <= max; i++)
            {
                if (i == max)
                {
                    iterThenCase = iter;

                    /** @todo All of this is super-ugly/-stupid/-slow/-inefficient! */

                    std::list<std::shared_ptr<TeroPattern>>::const_iterator iterPattern = (*iter)->GetPatterns().begin();

                    for (std::size_t j = 0U, maxJ = (*iter)->GetPatterns().size(); j < maxJ && iterPattern != (*iter)->GetPatterns().end(); j++)
                    {
                        if (j == nPatternMatch)
                        {
                            strPatternNameMatch = (*iterPattern)->GetPatternName();
                            break;
                        }
                        else
                        {
                            iterPattern++;
                        }
                    }

                    break;
                }
                else
                {
                    iter++;
                }
            }
        }

        const std::string& strThenCaseCode = (*iterThenCase)->GetThenCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strThenCaseCode == ".")
        {
            m_strCollector << strBuffer;
        }
        else
        {

        }

        /** @todo Make this pointer/reference, not copy! */
        m_strFunctionCurrent = (*iterThenCase)->GetThenCaseFunction();
    }
    else
    {
        const std::string& strElseCaseCode = iterFunction->second->GetElseCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strElseCaseCode == ".")
        {
            m_strCollector << strBuffer;
        }
        else
        {
            if (iterFunction->second->GetHasRetain() == true)
            {
                m_aStream.push(strBuffer);
            }
        }

        /** @todo Make this pointer/reference, not copy! */
        m_strFunctionCurrent = iterFunction->second->GetElseCaseFunction();
    }

    if (m_bAsync == false)
    {
        if (m_strFunctionPrevious != m_strFunctionCurrent)
        {
            std::unique_ptr<TeroEvent> pEvent(new TeroEvent(m_strFunctionPrevious,
                                                            m_strCollector.str(),
                                                            m_strFunctionCurrent,
                                                            strPatternNameMatch));
            m_aEvents.push(std::move(pEvent));

            m_strCollector.str("");
            m_strCollector.clear();

            return 0;
        }
        else
        {
            return 2;
        }
    }
    else
    {
        std::unique_ptr<TeroEvent> pEvent(new TeroEvent(m_strFunctionPrevious,
                                                        m_strCollector.str(),
                                                        m_strFunctionCurrent,
                                                        strPatternNameMatch));
        m_aEvents.push(std::move(pEvent));

        m_strCollector.str("");
        m_strCollector.clear();

        return 0;
    }
}

}
