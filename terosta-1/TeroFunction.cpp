/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of TeroStA.
 *
 * TeroStA is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * TeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with TeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroFunction.cpp
 * @author Stephan Kreutzer
 * @since 2021-07-18
 */


#include <stdexcept>
#include "TeroFunction.h"


namespace terosta
{

TeroFunction::TeroFunction(const std::string& strName,
                           std::unique_ptr<std::list<std::unique_ptr<TeroThenCase>>> pThenCases,
                           const std::string& strElseCaseCode,
                           const std::string& strElseCaseFunction,
                           bool bHasRetain):
  m_strName(strName),
  m_pThenCases(std::move(pThenCases)),
  m_strElseCaseCode(strElseCaseCode),
  m_strElseCaseFunction(strElseCaseFunction),
  m_bHasRetain(bHasRetain)
{
    if (m_strName.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty name string passed.");
    }

    if (m_pThenCases == nullptr)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction() with pThenCases == nullptr.");
    }

    if (m_pThenCases->size() <= 0U)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): pThenCases is empty.");
    }

    if (m_strElseCaseFunction.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty else-case function string passed.");
    }
}

const std::list<std::unique_ptr<TeroThenCase>>& TeroFunction::GetThenCases()
{
    return *m_pThenCases;
}

const std::string& TeroFunction::GetElseCaseCode()
{
    return m_strElseCaseCode;
}

const std::string& TeroFunction::GetElseCaseFunction()
{
    return m_strElseCaseFunction;
}

bool TeroFunction::GetHasRetain()
{
    return m_bHasRetain;
}

}
